#!/bin/bash

#echo $0
#echo $1
#echo $2
#echo $3
#echo $*
#echo $#

TEMPLATEHTMLFILE=/opt/iotproject/etc/index.html.template
INERVAL=20
HTMLOUTPUT=/opt/iotproject/index.html
DIRECTORYLIST=/tmp

if [ $# == 1 ]
then
    if [ -d "$1" ]
    then
        DIRECTORYLIST=$1
    else
        echo "Directory $1 not found" >&2
        exit 1

    fi

else
    echo "no Parameter"

fi
shift

counter=0
while [ $counter -lt20 ] ; do
    echo "<html><body>" > $HTMLOUTPUT
    echo "<h1>Mein Webserver</h1>" >> $HTMLOUTPUT
    date +%H:%M:%S >> $HTMLOUTPUT
    ls $DIRECTORYLIST >> $HTMLOUTPUT
    # Foor Loop der die existenz des Files prüft und dann anhängt und Fehlermeldung

    for htmlsnippet in $*

    do
    if [ -r "$htmlsnippet" ]
    then
    cat $htmlsnippet >> $HTMLOUTPUT 2>/dev/null
    else
        echo "file $htmlsnippet is not readable" >&2

done

    cat $* >> $HTMLOUTPUT
    echo "</body></html>" >> $HTMLOUTPUT
    sleep $INTERVAL
    counter=$(($counter+1))

done