#!/bin/bash

TEMPLATEHTMLFILE=/opt/iotproject/etc/index.html.template
INERVAL=20
HTMLOUTPUT=/opt/iotproject/index.html

mosquitto_sub -h localhost -t '#' -F "%t %p" | while read toppic payload ; do
    if [ "$topic" = "sensor/temp" ]
    then
    zeit=$(date)
    cat $TEMPLATEHTMLFILE | \
    sed s/_ZEIT_/$zeit/g | \
    sed "s/_TEMP_/$payload/g" > $HTMLOUTPUT
    fi
done